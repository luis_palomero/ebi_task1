Generate a file named 'part1.txt' with rows separated by '\n'.

Each row should start with letter 'A' followed by 5 numbers (e.g. A00001 or A33333). These are called accession numbers. Each accession number should randomly occur between 0 and 3 times with
uniform probability, e.g. A00001 could occur 0, 1, 2 or 3 times with 25% probability each. 