package com.luis.ebitests.task1.accession;

import com.luis.ebitests.task1.generator.IGenerator;

public class AccessionNumberGenerator {

	private int[] values;
	private float probability;
	private IGenerator generator;

	public AccessionNumberGenerator(int[] values, IGenerator generator) {
		this.values = values;
		this.probability = (float) (1.0 / values.length);
		this.generator = generator;

	}

	public float getProbability() {
		return this.probability;
	}

	public int[] getValues() {
		return this.values;
	}

	public IGenerator getGenerator() {
		return this.generator;
	}

	public double getRandom() {
		return Math.random();
	}

	public boolean equals(Object other) {
		if (other instanceof AccessionNumberGenerator == false) {
			return false;
		}
		AccessionNumberGenerator otherGenerator = (AccessionNumberGenerator) other;
		return (this.getValues().equals(otherGenerator.getValues())
				&& this.getGenerator().equals(otherGenerator.getGenerator()));
	}

	public int getNumber() {
		int accessIndex = (int) (this.generator.generate() * this.values.length);

		return this.values[accessIndex];
	}

}
