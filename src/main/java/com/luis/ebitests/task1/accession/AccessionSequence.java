package com.luis.ebitests.task1.accession;

public class AccessionSequence {
	private char key;
	private AccessionNumberGenerator[] generators;

	public AccessionSequence(char key, AccessionNumberGenerator... accessionNumberGenerators) {
		this.key = key;
		this.generators = accessionNumberGenerators;
	}

	public char getKey() {
		return this.key;
	}

	public AccessionNumberGenerator[] getGenerators() {
		return this.generators;
	}

	public String build() {
		StringBuilder sequence = new StringBuilder(Character.toString(this.key));
		for (AccessionNumberGenerator generator : this.generators) {
			sequence.append(generator.getNumber());
		}
		return sequence.toString();
	}

	public boolean equals(Object other) {
		if (other instanceof AccessionSequence == false) {
			return false;
		}
		AccessionSequence otherSequence = (AccessionSequence) other;
		if (otherSequence.getKey() != this.getKey()
				|| this.getGenerators().length != otherSequence.getGenerators().length) {
			return false;
		}
		for (int i = 0; i < this.getGenerators().length; i++) {
			if (this.getGenerators()[i].equals(otherSequence.getGenerators()[i]) == false) {
				return false;
			}
		}
		return true;

	}
}
