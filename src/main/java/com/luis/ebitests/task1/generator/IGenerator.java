package com.luis.ebitests.task1.generator;

public interface IGenerator {
	double generate();

	boolean equals(Object object);
}
