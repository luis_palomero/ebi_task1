package com.luis.ebitests.task1.generator;

public class UniformGenerator implements IGenerator {

	public double generate() {
		return Math.random();
	}

	public boolean equals(Object other) {
		return (other instanceof UniformGenerator);
	}

}
