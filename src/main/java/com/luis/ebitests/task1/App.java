package com.luis.ebitests.task1;

import java.io.FileWriter;
import java.io.IOException;

import com.luis.ebitests.task1.generator.IGenerator;
import com.luis.ebitests.task1.generator.UniformGenerator;

/**
 * Generates a file "part1.txt" some accession numbers A0000-A3333
 * 
 * Usage: java -jar name [linesToWrite]
 */
public class App {
	private static final int ACCESSIONS = 5;

	public static void main(String[] args) throws IOException {

		int linesToWrite;
		try {
			linesToWrite = Integer.parseInt(args[0]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Invalid parameters, parameters list should be [linesToWrite]");
			return;
		}

		String fileName = "part1.txt";

		System.out.println(
				"Starting Accession numbers generator for [" + linesToWrite + "] lines written at [" + fileName + "]");

		IGenerator uniform = new UniformGenerator();
		FileWriter fileWriter = new FileWriter(fileName);
		int[] numbers = { 0, 1, 2, 3 };
		Scenario scenario = new Scenario('A', ACCESSIONS, numbers, uniform, linesToWrite, fileWriter);
		scenario.init();
		scenario.run();

		System.out.println("Accesion numbers generated at [" + fileName + "]");

	}

}
