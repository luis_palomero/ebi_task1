package com.luis.ebitests.task1;

import java.io.FileWriter;
import java.io.IOException;

import com.luis.ebitests.task1.accession.AccessionNumberGenerator;
import com.luis.ebitests.task1.accession.AccessionSequence;
import com.luis.ebitests.task1.generator.IGenerator;

public class Scenario {

	private int numberOfAccessions;
	private char accessionKey;
	private int[] accessionDigits;
	private IGenerator random;
	private int numberOfLines;
	private AccessionSequence sequence;
	private FileWriter file;

	public Scenario(char key, int numberOfAccessions, int[] accessionDigits, IGenerator random, int numberOfLines,
			FileWriter file) {
		this.numberOfAccessions = numberOfAccessions;
		this.accessionKey = key;
		this.accessionDigits = accessionDigits;
		this.random = random;
		this.numberOfLines = numberOfLines;
		this.file = file;
	}

	public int getNumberOfLines() {
		return this.numberOfLines;
	}

	public AccessionSequence getAccessionSequence() {
		return this.sequence;
	}

	public FileWriter getFile() {
		return this.file;
	}

	public void init() {
		AccessionNumberGenerator[] generators = new AccessionNumberGenerator[this.numberOfAccessions];
		for (int i = 0; i < this.numberOfAccessions; i++) {
			generators[i] = new AccessionNumberGenerator(this.accessionDigits, this.random);
		}
		this.sequence = new AccessionSequence(this.accessionKey, generators);
	}

	public void run() {
		String code;
		try {
			for (int i = 0; i < this.numberOfLines; i++) {
				code = this.sequence.build().concat("\n");
				this.file.write(code);
			}
			this.file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
