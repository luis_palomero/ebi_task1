package com.luis.ebitests.task1.accession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.luis.ebitests.task1.generator.UniformGenerator;

public class AccessionNumberGeneratorTest {

	private static final double DELTA = 1e-15;
	private UniformGenerator random;

	@Before
	public void setUp() {
		random = new UniformGenerator();
	}

	@Test
	public void shouldReturnAllProbWithOneNumber() {
		// Arrange
		int[] numbers = { 1 };

		// Act
		AccessionNumberGenerator uniform = new AccessionNumberGenerator(numbers, random);

		// Assert
		assertEquals(1.0, uniform.getProbability(), DELTA);
	}

	@Test
	public void shouldReturnHalfProbWithTwoNumbers() {
		// Arrange
		int[] numbers = { 1, 2 };

		// Act
		AccessionNumberGenerator uniform = new AccessionNumberGenerator(numbers, random);

		// Assert
		assertEquals(0.5, uniform.getProbability(), DELTA);
	}

	@Test
	public void shouldReturnTheUniqueNumberWhenIsOne() {
		// Arrange
		int[] numbers = { 1 };
		AccessionNumberGenerator uniform = new AccessionNumberGenerator(numbers, random);

		// Act && Assert
		assertEquals(1, uniform.getNumber());
	}

	@Test
	public void shouldReturnTheFirstNumberWhenProbIsLow() {
		// Arrange
		int[] numbers = { 1, 2 };
		UniformGenerator mockGenerator = mock(UniformGenerator.class);
		when(mockGenerator.generate()).thenReturn(0.25);
		AccessionNumberGenerator uniform = new AccessionNumberGenerator(numbers, mockGenerator);

		// Act
		int result = uniform.getNumber();

		// Assert
		assertEquals(1, result);
	}

	@Test
	public void shouldReturnTheLastNumberWhenProbIsHigh() {
		// Arrange
		int[] numbers = { 1, 2 };
		UniformGenerator mockGenerator = mock(UniformGenerator.class);
		when(mockGenerator.generate()).thenReturn(0.75);
		AccessionNumberGenerator uniform = new AccessionNumberGenerator(numbers, mockGenerator);

		// Act
		int result = uniform.getNumber();

		// Assert
		assertEquals(2, result);
	}

	@Test
	public void shouldReturnValidResponsesAtEdgeValues() {
		// Arrange
		int[] numbers = { 1, 2, 3, 4 };
		UniformGenerator mockGenerator = mock(UniformGenerator.class);
		when(mockGenerator.generate()).thenReturn(0.24).thenReturn(0.25).thenReturn(0.26);
		AccessionNumberGenerator uniform = new AccessionNumberGenerator(numbers, mockGenerator);

		// Act
		int first = uniform.getNumber();
		int second = uniform.getNumber();
		int third = uniform.getNumber();

		// Assert
		assertEquals(1, first);
		assertEquals(2, second);
		assertEquals(2, third);
	}

}
