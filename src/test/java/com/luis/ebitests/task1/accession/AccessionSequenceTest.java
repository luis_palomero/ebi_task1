package com.luis.ebitests.task1.accession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class AccessionSequenceTest {

	private AccessionNumberGenerator mockGenerator;

	@Before
	public void setUp() throws Exception {
		mockGenerator = mock(AccessionNumberGenerator.class);
	}

	@Test
	public void shouldGenerateBasicSequenceWithoutNumbers() {
		// Arrange
		AccessionNumberGenerator[] generators = new AccessionNumberGenerator[0];
		AccessionSequence accession = new AccessionSequence('A', generators);

		// Act
		String sequence = accession.build();
		// Assert
		assertEquals("A", sequence);
	}

	@Test
	public void shouldGenerateBasicSequenceWithKeyAndDefinedNumbers() {
		// Arrange
		AccessionNumberGenerator[] generators = { mockGenerator, mockGenerator, mockGenerator };
		when(mockGenerator.getNumber()).thenReturn(1).thenReturn(2).thenReturn(3);
		AccessionSequence accession = new AccessionSequence('A', generators);

		// Act
		String sequence = accession.build();
		// Assert
		assertEquals("A123", sequence);
	}

}
