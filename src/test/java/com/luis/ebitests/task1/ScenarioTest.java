package com.luis.ebitests.task1;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.luis.ebitests.task1.accession.AccessionNumberGenerator;
import com.luis.ebitests.task1.accession.AccessionSequence;
import com.luis.ebitests.task1.generator.UniformGenerator;

public class ScenarioTest {

	private File file;
	private UniformGenerator uniformGenerator;

	@Before
	public void setUp() throws Exception {
		file = mock(File.class);
		uniformGenerator = mock(UniformGenerator.class);
	}

	@Test
	public void shouldInitializeAccessionSequence() throws IOException {
		// Assert
		int NUMBER_OR_ACCESSION_DIGITS = 5;
		int NUMBER_OF_ACCESSION_CODES_TO_WRITE = 5;
		int[] accessionDigits = { 0, 1, 2, 3 };
		AccessionNumberGenerator[] generators = { new AccessionNumberGenerator(accessionDigits, uniformGenerator),
				new AccessionNumberGenerator(accessionDigits, uniformGenerator),
				new AccessionNumberGenerator(accessionDigits, uniformGenerator),
				new AccessionNumberGenerator(accessionDigits, uniformGenerator),
				new AccessionNumberGenerator(accessionDigits, uniformGenerator) };

		AccessionSequence checkSequence = new AccessionSequence('A', generators);
		// //Act
		Scenario scenario = new Scenario('A', NUMBER_OR_ACCESSION_DIGITS, accessionDigits, uniformGenerator,
				NUMBER_OF_ACCESSION_CODES_TO_WRITE, new FileWriter("test.txt"));

		scenario.init();

		AccessionSequence realSequenceGenerator = scenario.getAccessionSequence();

		// Arrange
		assertEquals(checkSequence, realSequenceGenerator);

	}

	@Test
	public void shouldRunCompleteScenario() throws IOException {
		FileWriter mockFile = mock(FileWriter.class);

		int NUMBER_OR_ACCESSION_DIGITS = 5;
		int NUMBER_OF_ACCESSION_CODES_TO_WRITE = 5;
		int[] accessionDigits = { 0, 1, 2, 3 };
		AccessionNumberGenerator[] generators = { new AccessionNumberGenerator(accessionDigits, uniformGenerator),
				new AccessionNumberGenerator(accessionDigits, uniformGenerator),
				new AccessionNumberGenerator(accessionDigits, uniformGenerator),
				new AccessionNumberGenerator(accessionDigits, uniformGenerator),
				new AccessionNumberGenerator(accessionDigits, uniformGenerator) };

		AccessionSequence checkSequence = new AccessionSequence('A', generators);
		// Act
		Scenario scenario = new Scenario('A', NUMBER_OR_ACCESSION_DIGITS, accessionDigits, uniformGenerator,
				NUMBER_OF_ACCESSION_CODES_TO_WRITE, mockFile);

		scenario.init();
		scenario.run();
		verify(mockFile, times(5)).write(any(String.class));
	}

}
